package com.vadim.demoapp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author vadim
 */
@Controller
public class ViewController {
    
    @GetMapping("/")
    public Object index() {
        return "index";
    }
    
}
